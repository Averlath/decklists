let reiya = [
	"Taegrus Pearlshine, Lord of the Mountain",
	"Kirik Rerik, the Draconic Warrior",
	"Shaela, the Mermaid Princess",
	"Gill, the Gifted Conjurer",
	"Reiya, Fourth Daughter of the Mikage",
	"Frayla, the Revolutionist",
	"Faerur Letoliel, King of Wind",
	"Pandora, Guardian of the Sacred Temple",
	"Ayu, Shaman Swordswoman",
	"Welser, King of Demons",
	"Scarlet, the Crimson Beast",
	"Scheherazade of the Catastrophic Nights",
	"Aimul, Princess of Despair",
	"Gil Alhama'at, Treasonous Emperor",
	"Phantom Wind Fiethsing",
	"The Time Spinning Witch"
]
let valhalla = [
	"Ayu, Shaman Swordswoman",
	"Brunhild, Caller of Spirits",
	"Atom Seikhart, the Shimmering Rabbit",
	"Isis, the Hundred Weapon Master",
	"Fu Xi, King of Kunlun",
	"Loki, the Witch of Chaos",
	"Arthur, King of Machines",
	"Hanzo, Chief of the Kouga",
	"Chamimi, Guardian of the Sacred Bow",
	"Lucifer, Fallen Angel of Sorrow",
	"Lich, the Saint of Death"
]