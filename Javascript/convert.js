let myFile = {};
let execmd = true, execsd = true, execside = true, execrunes = true;

window.onload = function() {
    document.getElementById('input').addEventListener("change", getFile);
    document.getElementById("new").style.display = "none";
    //document.getElementById("untap").style.display = "none";
}

function getFile(event) {
    const input = event.target;
    if ('files' in input && input.files.length > 0) {
        const reader = new FileReader();
        return new Promise((resolve, reject) => {
            reader.onload = function(event) {
                document.getElementById("back").style.display = "none";
                document.getElementById("input").style.display = "none";
                document.getElementById("new").style.display = "block";
                document.getElementById("untap").style.display = "block";
                checkExistance(event.target.result);
                printDeck(event.target.result);
                myFile.file = event.target.result;
            }
            reader.readAsText(input.files[0]);
        });
    }
}

function reload() {
    location.reload();
}

function checkExistance(file) {
    if (file.indexOf("Ruler x ") === -1 ||
        file.indexOf("Main Deck x ") === -1 ||
        file.indexOf("Stone Deck x ") === -1) {
        throwError("Missing Ruler, Main Deck or Stone Deck.");
    }
}

function throwError(error) {
    $("#ruler").empty();
    $("#maindeck").empty();
    $("#stones").empty();
    $("#sideboard").empty();
    $("#runes").empty();
    create("h3", error, ruler);
    throw error;
}

function printDeck(file) {
    const getJ = new getRuler(file);

    let ruler = document.getElementById("ruler");
    create("h2", "Ruler", ruler);
    create("p", getRuler(file), ruler);

    let maindeck = document.getElementById("maindeck");
    create("h3", "Main Deck - " + getCap(file, file.indexOf("Main Deck x ") + 12), maindeck);
    getMainDeck(file);

    let stonesdeck = document.getElementById("stones");
    create("h3", "Stones Deck - " + getCap(file, file.indexOf("Stone Deck x ") + 13), stonesdeck);
    getStonesDeck(file);

    if (parseInt(getCap(file, file.indexOf("Sideboard x ") + 12)) !== 0) {
        let sideboard = document.getElementById("sideboard");
        create("h3", "Sideboard - " + getCap(file, file.indexOf("Sideboard x ") + 12), sideboard);
        getSideDeck(file);
    }

    for (let item of valhalla) {
        if (item === getJ.jruler() && file.indexOf("Runes Deck x ") >= 0) {
            let runes = document.getElementById("runes");
            create("h3", "Runes Deck - " + getCap(file, file.indexOf("Runes Deck x ") + 13), runes);
            getRunesDeck(file);
        }
    }

    for (let item of valhalla) {
        if (item === getJ.jruler() && file.indexOf("Runes Deck x ") <= 0) {
            let runes = document.getElementById("runes");
            create("h3", "Detected a ruler, '" + getRuler(file) + "' who is able to use Runes" + 
            ", but there is no Runes Deck. <br> If your deck has runes, please make sure that " + 
            "it follows our format:", runes);
            create("p", "Runes Deck x 5", runes);
            create("p", "Name-of-the-Rune x 1", runes)
        }
    }
}

function create(type, html, overlord) {
    let item = document.createElement(type);
    item.innerHTML = html;
    overlord.append(item);
}

function getRuler(file) {
    let start = file.indexOf("Ruler x ") + 11;
    let str, result = "",  deck = [], startj = start, end = start + 1, number = 0, n=2;
    let cap = getCap(file, file.indexOf("Ruler x ") + 8);

    for (let i = start; i < end; i++) {
        str = "";
        if (deck.length === parseInt(cap) || deck.length >= 3) {
            break;
        }
        for (let j = startj; j < nth(file, " x ", n); j++) {
            str += file[j];
        }
        deck.push(str);
        end = file.indexOf(str) + str.length + 6;
        startj = file.indexOf(str) + str.length + 6;
        n++;
    }

    for (item of deck) {
        result += item + " / ";
    }

    this.jruler = function() {
        return deck[1];
    }
    this.total = function() {
        return deck.length;
    }

    deckCheck("Ruler", cap);

    return result.slice(0, -2);
}

function getMainDeck(file, boolean) {
    const ruler = new getRuler(file);

    let deck = {};
    let start = file.indexOf("Main Deck x ") + 16;
    let cap = getCap(file, file.indexOf("Main Deck x ") + 12);
    let n = 3 + ruler.total();

    fillDeck(file, deck, start, cap, n);

    this.total = countKeys(deck);
    deckCheck("Main Deck", cap);

    if (execmd === true) {
        execmd = false;
        return readObj(deck, "maindeck");
    }

    if (boolean === true) {
        return deck;
    }
}

function getStonesDeck(file, boolean) {
    const ruler = new getRuler(file);
    const md = new getMainDeck(file);

    let deck = {};
    let start = file.indexOf("Stone Deck x ") + 17;
    let cap = getCap(file, file.indexOf("Stone Deck x ") + 13);
    let n = 4 + ruler.total() + md.total;

    fillDeck(file, deck, start, cap, n);
    this.total = countKeys(deck);
    deckCheck("Stone Deck", cap);

    if (execsd === true) {
        execsd = false;
        return readObj(deck, "stones");
    }

    if (boolean === true) {
        return deck;
    }
}

function getSideDeck(file, boolean) {
    const ruler = new getRuler(file);
    const md = new getMainDeck(file);
    const sd = new getStonesDeck(file);

    let deck = {};
    let start = checkSide(file);
    let cap = getCap(file, file.indexOf("Sideboard x ") + 12);
    let n = 5 + ruler.total() + md.total + sd.total;

    fillDeck(file, deck, start, cap, n);
    this.total = countKeys(deck);

    if (execside === true) {
        execside = false;
        return readObj(deck, "sideboard");
    }

    if (boolean === true) {
        return deck;
    }
}

function checkSide(file) {
    if (parseInt(getCap(file, file.indexOf("Sideboard x ") + 11)) <= 9 &&
        parseInt(getCap(file, file.indexOf("Sideboard x ") + 11)) === "") {
        return file.indexOf("Sideboard x ") + 15;
    } else {
        return file.indexOf("Sideboard x ") + 16;
    }
}

function getRunesDeck(file, boolean) {
    const ruler = new getRuler(file);
    const md = new getMainDeck(file);
    const sd = new getStonesDeck(file);
    const side = new getSideDeck(file);

    let deck = {};
    let start = file.indexOf("Runes Deck x ") + 16
    let cap = getCap(file, file.indexOf("Runes Deck x ") + 13);
    let n = 6 + ruler.total() + md.total + sd.total + side.total;

    fillDeck(file, deck, start, cap, n);
    deckCheck("Runes Deck", cap);

    if (execrunes === true) {
        execrunes = false;
        return readObj(deck, "runes");
    }

    if (boolean === true) {
        return deck;
    }
}

function fillDeck(file, deck, start, cap, n) {
    let str, card, variant, variantcount, startj = start, end = start + 1, number = 0;

    for (let i=start; i<end; i++) {
        str = "";
        if (countValues(deck) >= parseInt(cap)) {
            break;
        }
        for (let j = startj; j < nth(file, " x ", n); j++) {
            str += file[j];
        }
        if (allowedCopies(str, file[file.indexOf(str) + str.length + 3])) {
            if (str.indexOf("�yVariant�z") !== -1 ) {
                variant = str;
                variantcount = file[file.indexOf(str) + str.length + 3];
            } else if (parseInt(cap) - countValues(deck) === parseInt(variantcount)) {
                variant = variant.replace("�yVariant�z", "[Variant] ");
                deck[variant] = variantcount;
            } else {
                card = str.replace("�yVariant�z", "[Variant] ");
                deck[card] = file[file.indexOf(str) + str.length + 3];
            }
            end = file.indexOf(str) + str.length + 6;
            startj = file.indexOf(str) + str.length + 6;
            n++;
        } else {
            throwError("Unallowed amount of copies of a card.");
        }
    }
}

function countKeys(deck) {
    let keys = 0;
    Object.keys(deck).forEach(function(key) {
        keys ++;
    });

    return keys;
}

function countValues(deck) {
    let values = 0;
    Object.keys(deck).forEach(function(key) {
        values += parseInt(deck[key]);
    });

    return values;
}

function allowedCopies(str, amount) {
    let exists = false;
    if (amount > 4) {
        for (item of allowed) {
            if (str === item) {
                exists = true;
            }
        }
    } else {
        return true;
    }

    return exists ? true : false;
}

function deckCheck(type, length) {
    switch (type) {
        case "Ruler":
            if (3 < length || length < 1) {
                console.log("Here");
                throwError("Incorrect Ruler Area");
            }
            break;
        case "Main Deck":
            if (60 < length || length < 40) {
                throwError("Incorrect Main Deck Area");
            }
            break;
        case "Stone Deck":
            if (20 < length || length < 10) {
                throwError("Incorrect Stone Deck Area");
            }
            break;
        case "Runes Deck":
            if (5 < length) {
                throwError("Incorrect Runes Deck Area");
            }
            break;
    }
}

function getCap(file, len) {
    let cap = "";
    for (let i=len;; i++) {
        if (cap.length === 2) {
            break;
        }
        cap += file[i];
    }

    return cap;
}

function nth(file, pattern, n) {
    let i = -1;
    while (n-- && i++ < file.length) {
        i = file.indexOf(pattern, i);
        if (i < 0) {
            break;
        }
    }

    return i;
}

function readObj(deck, div) {
    let item = document.getElementById(div), i = 1;
    Object.keys(deck).sort().forEach(function(key) {
        let p = document.createElement("p");
        p.setAttribute("class", "card");
        p.innerHTML = i + ". " + key + " - x" + deck[key];
        item.appendChild(p);
        i++;
    });
}

function untap() {
    download("testing.txt", write(myFile.file));
}

function write(file) {
    let context = "", side, runes, sideEx = false, runesEx = false;
    const getJ = new getRuler(file);
    let ruler = getRuler(file);
    let deck = getMainDeck(file, true);
    let stones = getStonesDeck(file, true);
    if (parseInt(getCap(file, file.indexOf("Sideboard x ") + 12)) != 0) {
        side = getSideDeck(file, true);
        sideEx = true;
    }
    for (let item of valhalla) {
        if (item === getJ.jruler() && file.indexOf("Runes Deck x ") !== 0) {
            runes = getRunesDeck(file, true);
            runesEx = true;
        }
    }

    context += "//play-1\r\n";
    context += "1 " + ruler.replace("/", "-") + "\r\n\r\n";
    context += "//deck-1\r\n";

    Object.keys(deck).sort().forEach(function(key) {
        context += deck[key] + " " + key + "\r\n";
    });

    context += "\r\n//deck-2\r\n";

    Object.keys(stones).sort().forEach(function(key) {
        context += stones[key] + " " + key + "\r\n";
    });

    context += "\r\n//sideboard-1\r\n";

    if (sideEx) {
        Object.keys(side).sort().forEach(function(key) {
            context += side[key] + " " + key + "\r\n";
        });
    }

    context += "\r\n//pile-facedown\r\n";

    if (runesEx) {
        Object.keys(runes).sort().forEach(function(key) {
            context += runes[key] + " " + key + "\r\n";
        });
    }

    return context.slice(0, -2);
}

function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

/* Things to do:
* Attempt to implement the " x " system, with which language won't matter
* Implement the option of using Croco-Shark and Kimono (2 digits copies in deck)
 */