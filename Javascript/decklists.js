let format, ruler;
window.onload = function() {
	$('#format').change(function() {
		format = $('#format').val();
		loadRulers(format);
	});

	$('#rulers').change(function() {
		ruler = $('#rulers').val();
		loadDecks(ruler, format);
	})
};

function loadRulers(format) {
	clearSelect();
	document.getElementById("back").style.display = "none";
	let option = document.createElement("option");
	option.text = "Select a ruler";
	document.getElementById("rulers").add(option);
	switch (format) {
		case "New Frontier: Reiya/Valhalla":
			for (item of reiya) {
				let option = document.createElement("option");
				option.text = item;
				document.getElementById("rulers").add(option);
			}
			for (item of valhalla) {
				let option = document.createElement("option");
				option.text = item;
				document.getElementById("rulers").add(option);
			}
			break;
		case "Cluster: Valhalla":
			for (item of valhalla) {
				let option = document.createElement("option");
				option.text = item;
				document.getElementById("rulers").add(option);
			}
			break;
		case "Cluster: Reiya":
			for (item of reiya) {
				let option = document.createElement("option");
				option.text = item;
				document.getElementById("rulers").add(option);
			}
			break;
		case "New Frontier: Lapis/Reiya": case "Wanderer":
			let option = document.createElement("option");
			option.text = "Not yet implemented";
			document.getElementById("rulers").add(option);
			break;
	}
}

function loadDecks(ruler, format) {
	createTable();
	let deck = 0;
	for (let i=0; i<decks.length; i++) {
		if (decks[i].ruler == ruler && decks[i].format === format) {
			deck ++;
			printDeck(deck, ruler, decks[i].image, decks[i].note, decks[i].expansions);
		}
	}
	if (deck === 0) {
		createMissing(ruler);
	}
}

function createTable() {
	clearDiv();
	let pform = document.getElementById("deckspot");

	let div = document.createElement("div");
	div.setAttribute("id", "wrapper");
	pform.appendChild(div);

	let table = document.createElement("table");
	table.setAttribute("id", "table");
	pform.appendChild(table);

	let tr = document.createElement("tr");
	table.appendChild(tr);

	let thzero = document.createElement("th");
	thzero.setAttribute("class", "th");
	thzero.innerHTML = "#";
	tr.appendChild(thzero);

	let th = document.createElement("th");
	th.setAttribute("class", "th");
	th.innerHTML = "Ruler";
	tr.appendChild(th);

	let thone = document.createElement("th");
	thone.setAttribute("class", "th");
	thone.innerHTML = "Image";
	tr.appendChild(thone);

	let thtwo = document.createElement("th");
	thtwo.setAttribute("class", "th");
	thtwo.innerHTML = "Expansions";
	tr.appendChild(thtwo);

	let three = document.createElement("th");
	three.setAttribute("class", "th");
	three.innerHTML = "Note";
	tr.appendChild(three);
}

function printDeck(id, ruler, image, note, expansions) {
	let tr = document.createElement("tr");
	document.getElementById("table").appendChild(tr);

	let tdzero = document.createElement("td");
	tdzero.setAttribute("class", "td");
	tdzero.innerHTML = id;
	tr.appendChild(tdzero);

	let td = document.createElement("td");
	td.setAttribute("class", "td");
	td.innerHTML = ruler;
	tr.appendChild(td);

	let tdtwo = document.createElement("td");
	tdtwo.setAttribute("class", "td");
	tdtwo.innerHTML = image;
	tr.appendChild(tdtwo);

	let tdthree = document.createElement("td");
	tdthree.setAttribute("class", "td");
	tdthree.innerHTML = expansions;
	tr.appendChild(tdthree);

	let tdfour = document.createElement("td");
	tdfour.setAttribute("class", "td");
	tdfour.innerHTML = note;
	tr.appendChild(tdfour);
}

function createMissing(ruler) {
	clearDiv();
	let pform = document.getElementById("deckspot");

	let div = document.createElement("div");
	div.setAttribute("id", "wrapper");
	pform.appendChild(div);

	let h2 = document.createElement("h2");
	h2.setAttribute("id", "missing");
	h2.innerHTML = "There are no '" + ruler + "' decks at this time.";
	div.appendChild(h2);
}

function clearSelect() {
	$("#rulers").empty();
}

function clearDiv() {
	$("#deckspot").empty();
}